package com.bdi.sb.mapper;

import java.util.List;
import java.util.Map;

import com.bdi.sb.vo.BoardInfoVO;
import com.bdi.sb.vo.PageVO;

public interface BoardInfoMapper {

	public List<BoardInfoVO> selectBoardInfoList(BoardInfoVO board,PageVO page);
	public BoardInfoVO selectBoardInfo(BoardInfoVO board);
	public int insertBoardInfo(BoardInfoVO board);
	public int deleteBoardInfo(BoardInfoVO board);
	public int updateBoardInfo(BoardInfoVO board);
}
