package com.bdi.sb.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bdi.sb.service.TestInfoService;
import com.bdi.sb.vo.TestInfoVO;

@Controller//RestController는 무조건 JSON형태로 내보냄 Controller는 String형태라면 바로 주소로 간다
@RequestMapping("/testv")//이게 있으면 무조건 이거먼저 타고 아래로 내려가야한다
public class TestInfoViewController {

	@Resource//javax거라 spring을 빼도 사용이 가능하다
	public TestInfoService tiService;
	
	@GetMapping("tests")
	public String getTestInfoList(@ModelAttribute TestInfoVO test, Model m) {//PostMapping이라면 RequestBody를 사용해서 JSON형태로 주거나 다른형식으로 form데이터로 보내야함
		m.addAttribute("rMap",tiService.selectTestInfoList(test));//Model도 request스코프를 사용한다
		return "/views/test/list";
	}
}
