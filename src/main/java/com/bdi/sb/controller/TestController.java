package com.bdi.sb.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bdi.sb.mapper.UserInfoMapper;
import com.bdi.sb.vo.UserInfoVO;

@RestController
public class TestController {

	@Resource
	private UserInfoMapper uiMapper;
	
	
	@GetMapping("/hello")
	public String hello() {
		return "hello";
	}
	@GetMapping("/hellos")
	public List<String> hellos(){
		List<String> strList = new ArrayList<>();
		strList.add("1");
		strList.add("2");
		strList.add("짝");
		strList.add("4");
		strList.add("5");
		return strList;
	}
	@GetMapping("hellomap") 
	public Map<String,String> helloMap(){
		Map<String,String> rMap = new HashMap<>();
		rMap.put("1", "일");
		rMap.put("2", "이");
		rMap.put("3", "삼");
		rMap.put("4", "사");
		rMap.put("5", "오");
		return rMap;
	}
}
