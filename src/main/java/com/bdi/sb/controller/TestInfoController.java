package com.bdi.sb.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bdi.sb.service.TestInfoService;
import com.bdi.sb.vo.TestInfoVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class TestInfoController {

	@Resource
	private TestInfoService tiService;
	
	@GetMapping("/tests")
	public Map<String,Object> getTests(@ModelAttribute TestInfoVO test){
		log.info("getTests=>{}",test);
		return tiService.selectTestInfoList(test);
	}
	
	@GetMapping("/test")
	public TestInfoVO getTest(@ModelAttribute TestInfoVO test){
		log.info("getTest=>{}",test);
		return tiService.selectTestInfo(test);
	}
	
	@GetMapping("/delete")
	public Map<String,String> deleteTest(@ModelAttribute TestInfoVO test){
		log.info("deleteTest=>{}",test);
//		return null;
		return tiService.deleteTestInfo(test);
	}
	
	@PostMapping("/update")
	public Map<String,String> updateTest(@RequestBody TestInfoVO test){
		log.info("updateTest=>{}",test);
		return tiService.updateTestInfo(test);
	}
	
	@PostMapping("/tests")
	public Map<String,String> insertTest(@RequestBody TestInfoVO test){
		log.info("insertTest=>{}",test);
		return tiService.insertTestInfo(test);
	}
	
	@PostMapping("/tt")
	public Map<String,Object> getTest1(@RequestBody TestInfoVO test){
		log.info("tt=>{}",test);
		return tiService.selectTestInfoList(test);
	}
}
