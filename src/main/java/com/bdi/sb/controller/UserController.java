package com.bdi.sb.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bdi.sb.service.UserInfoService;
import com.bdi.sb.vo.UserInfoVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class UserController {
	
	@Resource
	private UserInfoService uiService;
	
	@GetMapping("/users")
	public List<UserInfoVO> getUserInfoList(UserInfoVO user){
		log.info("나는 2번째");
		return uiService.selectUserInfoList(null);
	}
	
	@PostMapping("/user/login")
	public UserInfoVO doLogin(@ModelAttribute UserInfoVO user) {
		return uiService.doLogin(user);
	}
	
	@PostMapping("/user/insert")
	public int insertUserInfo(UserInfoVO user) {
		return uiService.insertUserInfo(user);
	}
//	@Resource
//	private UserInfoMapper uiMapper;
//	
//	@GetMapping("/users/login")//로그인
//	public List<UserInfoVO> selectUser(UserInfoVO ui){
//		return uiMapper.selectUser(ui);
//	}
//	
//	@GetMapping("/users/selectList")//전체 가져오기
//	public List<UserInfoVO> selectUserInfoList(UserInfoVO ui){
//		return uiMapper.selectUserInfoList(null);
//	}
//	
//	@GetMapping("/users/selectOne")//조회
//	public List<UserInfoVO> selectUserInfo(UserInfoVO ui){
//		return uiMapper.selectUserInfo(ui);
//	}
//	
//	@PostMapping("/users/insert")//회원가입
//	public Map<String,String> insertUserInfo(UserInfoVO ui) {
//		Map<String,String> rMap = new HashMap<>();
//		if(uiMapper.insertUserInfo(ui)==1) {
//		rMap.put("msg", "회원가입에 성공하셨습니다");
//		}
//		return rMap;
//	}
//	
//	@PostMapping("/users/update")//회원정보수정
//	public Map<String,String> updateUserInfo(UserInfoVO ui) {
//		Map<String,String> rMap = new HashMap<>();
//		if(uiMapper.updateUserInfo(ui)==1) {
//			rMap.put("msg", "회원 정보 수정이 완료되었습니다");
//			}
//		return rMap;
//	}
//	
//	@GetMapping("/users/delete")//회원 삭제
//	public Map<String,String> deleteUserInfo(UserInfoVO ui) {
//		Map<String,String> rMap = new HashMap<>();
//		if(uiMapper.deleteUserInfo(ui)==1) {
//			rMap.put("msg", "회원탈퇴 하셨습니다");
//			}
//		return rMap;
//	}
}
