package com.bdi.sb.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.bdi.sb.mapper.BoardInfoMapper;
import com.bdi.sb.service.BoardInfoService;
import com.bdi.sb.vo.BoardInfoVO;
import com.bdi.sb.vo.PageVO;

@Service
public class BoardInfoServiceImpl implements BoardInfoService {
	
	@Resource
	private BoardInfoMapper biMapper;

	@Value("${save.file.path}")
	private String saveFilePath;
	
	@Override
	public List<BoardInfoVO> selectBoardInfoList(BoardInfoVO board, PageVO page) {
		page.setStartNum((page.getPage()-1) * 10);
		return biMapper.selectBoardInfoList(board, page);
	}

	@Override
	public BoardInfoVO selectBoardInfo(BoardInfoVO board) {
		return null;
	}

	@Override
	public Map<String, Object> insertBoardInfo(BoardInfoVO board) {
		Map<String, Object> rMap = new HashMap<>();
		if(biMapper.insertBoardInfo(board)==1) {
			rMap.put("msg", "성공");
		}
		MultipartFile mf = board.getBiFileItem();
		if(mf!=null) {
			String extName = FilenameUtils.getExtension(mf.getOriginalFilename());//filename은 test.gif getExtension은 .gif 즉 확장자명만 잘라서 보내줌
			String fileName = System.nanoTime() + "." + extName;//이름에 나노타임
			board.setBiFile(fileName);
			String filePath = saveFilePath + fileName;
			File targetFile = new File(filePath);
			try {
				Files.copy(mf.getInputStream(), targetFile.toPath(), StandardCopyOption.REPLACE_EXISTING);//있으면 덮어쓰는것 하지만 나노타임으로 만들었기 때문에 그럴일 없음
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}	
		return rMap;
	}

	@Override
	public Map<String, Object> deleteBoardInfo(BoardInfoVO board) {
		return null;
	}

	@Override
	public Map<String, Object> updateBoardInfo(BoardInfoVO board) {
		return null;
	}

}
