package com.bdi.sb.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.bdi.sb.mapper.TestInfoMapper;
import com.bdi.sb.service.TestInfoService;
import com.bdi.sb.vo.PageVO;
import com.bdi.sb.vo.TestInfoVO;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TestInfoServiceImpl implements TestInfoService {

	@Resource
	private TestInfoMapper tiMapper;

	@Override
	public Map<String, Object> selectTestInfoList(TestInfoVO test) {
		int startNum = test.getPage().getPage() * 10 - 10;
		test.getPage().setStartNum(startNum);
		Integer totalCount = tiMapper.totalTestInfoCount(test);
		Map<String, Object> rMap = new HashMap<>();
		rMap.put("list", tiMapper.selectTestInfoList(test));
		PageVO page = test.getPage();
		page.setTotalCount(totalCount);
		rMap.put("page", page);
		log.info("rMap=>{}", rMap);
		return rMap;
	}

	@Override
	public Map<String, String> insertTestInfo(TestInfoVO test) {
		Map<String, String> rMap = new HashMap<>();
		rMap.put("msg", "등록 완료 되었습니다");
		if (tiMapper.insertTestInfo(test) != 1) {
			rMap.put("msg", "알수 없는 오류입니다");
		}
		return rMap;
	}

	@Override
	public TestInfoVO selectTestInfo(TestInfoVO test) {
		return tiMapper.selectTestInfo(test);
	}

	@Override
	public Map<String, String> deleteTestInfo(TestInfoVO test) {
		Map<String, String> rMap = new HashMap<>();
		rMap.put("msg", "삭제 되었습니다");
		if (tiMapper.deleteTestInfo(test) == 0) {
			rMap.put("msg", "알수 없는 오류입니다");
		}
		return rMap;
	}

	@Override
	public Map<String, String> updateTestInfo(TestInfoVO test) {
		Map<String, String> rMap = new HashMap<>();
		rMap.put("msg", "알수 없는 오류입니다");
		if (tiMapper.updateTestInfo(test) == 1) {
			if (tiMapper.updateTiUpdcnt(test) == 1) {
				rMap.put("msg", "수정 되었습니다");
			}
		}
		return rMap;
	}

}
