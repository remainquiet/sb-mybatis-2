package com.bdi.sb.service;

import java.util.Map;

import com.bdi.sb.vo.TestInfoVO;

public interface TestInfoService {
	
	public Map<String,Object> selectTestInfoList(TestInfoVO test);
	public Map<String,String> insertTestInfo(TestInfoVO test);
	public TestInfoVO selectTestInfo(TestInfoVO test);
	public Map<String,String> deleteTestInfo(TestInfoVO test);
	public Map<String,String> updateTestInfo(TestInfoVO test);
}
