package com.bdi.sb.service;

import java.util.List;

import com.bdi.sb.vo.UserInfoVO;

public interface UserInfoService {

	public List<UserInfoVO> selectUserInfoList(UserInfoVO ui);
	public List<UserInfoVO> selectUserInfo(UserInfoVO ui);
	public UserInfoVO doLogin(UserInfoVO ui);
	public int insertUserInfo(UserInfoVO ui);
	public int updateUserInfo(UserInfoVO ui);
	public int deleteUserInfo(UserInfoVO ui);
}
