package com.bdi.sb.service;

import java.util.List;
import java.util.Map;

import com.bdi.sb.vo.BoardInfoVO;
import com.bdi.sb.vo.PageVO;

public interface BoardInfoService {

	public List<BoardInfoVO> selectBoardInfoList(BoardInfoVO board, PageVO page);
	public BoardInfoVO selectBoardInfo(BoardInfoVO board);
	public Map<String,Object> insertBoardInfo(BoardInfoVO board);
	public Map<String,Object> deleteBoardInfo(BoardInfoVO board);
	public Map<String,Object> updateBoardInfo(BoardInfoVO board);
}
