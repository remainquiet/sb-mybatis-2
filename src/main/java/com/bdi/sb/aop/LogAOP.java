package com.bdi.sb.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Aspect
@Slf4j
public class LogAOP {
	@Before("execution(public String com.bdi.sb.controller..*(..))")
	public void aop1Before(JoinPoint jp) {
		log.info("나는 aop1");
		log.info("jp=>{}",jp);
	}
	
	@Before("execution(private void com.bdi.sb.controller..*(..))")
	public void aop2Before(JoinPoint jp) {
		log.info("나는 aop2");
		log.info("jp=>{}",jp);
	}
	
	@Before("execution(public java.util.List<String> com.bdi.sb.controller..*(..))")
	public void aop3Before(JoinPoint jp) {
		log.info("나는 aop3");
		log.info("jp=>{}",jp);
	}
	
	@Before("execution(private java.util.List<String> com.bdi.sb.controller..*(..))")
	public void aop4Before(JoinPoint jp) {
		log.info("나는 aop4");
		log.info("jp=>{}",jp);
	}
}
