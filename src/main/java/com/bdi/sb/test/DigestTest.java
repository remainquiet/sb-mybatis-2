package com.bdi.sb.test;

import cc.duduhuo.util.digest.Digest;

public class DigestTest {
	final static String SALT_KEY="parkdoyoung123456";

	public static void main(String[] args) {
		String pwd = "wmdmk2dj2";
		String sha265Pwdsort = Digest.sha256Hex(pwd+SALT_KEY);
		String sha265Pwd = Digest.sha256Hex(pwd);
		
		System.out.println(sha265Pwd);
		System.out.println(sha265Pwdsort);
	}
}
