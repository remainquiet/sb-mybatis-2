<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="/js/jquery-3.4.1.min.js"></script>
<script src="/js/bootstrap.bundle.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/css/bootstrap-grid.min.css">
<link rel="stylesheet" href="/css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="/css/bootstrap.min.css">
</head>
<body>
	<table class="table table-bordered">
		<tr>
			<th><input type="checkbox"></th>
			<th>번호</th>
			<th>이름</th>
			<th>주소</th>
			<th>비고</th>
		</tr>

		<c:forEach items="${rMap.list}" var="li">
			<tr>
				<td><input type="checkbox" value="${li.tiNum}"></td>
				<td>${li.tiNum}</td>
				<td>${li.tiName}</td>
				<td>${li.tiAddr}</td>
				<td>${li.tiEtc}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>