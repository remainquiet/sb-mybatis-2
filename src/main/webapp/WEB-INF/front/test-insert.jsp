<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
게시물작성
<!-- 

이름 : 2글자 이상 100글자 이하 Null 불가능
주소 : 10글자 이상 200글자 이하 Null 불가능
비고 : 1000글자 이하 Null 가능

 -->
<table class="table table-bordered">
	<tr>
		<th>이름</th>
		<td><input type="text" name="tiName" id="tiName" maxlength="100"></td>
	</tr>
	<tr>
		<th>주소</th>
		<td><input type="text" name="tiAddr" id="tiAddr" maxlength="200"></td>
	</tr>
	<tr>
		<th>비고</th>
		<td><textarea name="tiEtc" id="tiEtc" maxlength="1000" cols="30"
				rows="10"></textarea></td>
	</tr>
	<tr>
		<th colspan="2"><button id="tiInsert">등록</button></th>
	</tr>
</table>
<script>
	$(document).ready(function() {
		$('#tiInsert').on('click', function() {

			var tiName = $('#tiName').val();
			var tiAddr = $('#tiAddr').val();
			var tiEtc = $('#tiEtc').val();

			if (tiName.trim().length < 2) {
				alert('이름은 2글자 이상이여야 합니다');
				return false;
			}
			if (tiAddr.trim().length < 10) {
				alert('주소는 10글자 이상이여야 합니다');
				return false;
			}
			console.log(tiName);
			console.log(tiAddr);
			console.log(tiEtc);

			$.ajax({
				url : '/tests',
				method : 'POST',
				contentType : 'application/json; charset=utf-8',
				data : JSON.stringify({
					tiName : tiName,
					tiAddr : tiAddr,
					tiEtc : tiEtc
				}),
				success : function(res) {
					console.log(res);
					alert(res.msg);
					location.href = '/?page=test-list';
				}

			})
		})

	})
</script>