<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="/js/jquery-3.4.1.min.js"></script>
<script src="/js/bootstrap.bundle.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/css/bootstrap-grid.min.css">
<link rel="stylesheet" href="/css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="/css/bootstrap.min.css">

<style>
a {
	cursor: pointer;
}

a[data-page] {
	color: #1297e4;
}

a[data-page]:hover {
	font-weigt : bold;
	color: orange;
	cursor: pointer;
}
</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
		<div class="container">
			<a class="navbar-brand" href="#"> <img
				src="http://placehold.it/150x50?text=Logo" alt="">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarResponsive" aria-controls="navbarResponsive"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active" data-page="main"><a href="/?page=main"
						class="nav-link">Home <span class="sr-only">(current)</span>
					</a></li>
					<li class="nav-item active" data-page="login"><a href="/?page=login"
						class="nav-link">로그인</a></li>
					<li class="nav-item active" data-page="join"><a href="/?page=join"
						class="nav-link">회원가입</a></li>
					<li class="nav-item active" data-page="test-list"><a href="/?page=test-list"
						class="nav-link">리스트</a></li>
					<li class="nav-item active" data-page="contact"><a href="/?page=contanct"
						class="nav-link">뭐고 이거</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container">
		<c:if test="${empty param.page}">
			<jsp:include page="/WEB-INF/front/main.jsp"></jsp:include>
		</c:if>
		<c:if test="${not empty param.page}">
			<jsp:include page="/WEB-INF/front/${param.page}.jsp"></jsp:include>
		</c:if>
	</div>
	<script>
		$(document).ready(function() {
			var pPage = '${param.page}';

			$('li[data-page]').removeClass('active');
			$('li[data-page=' + pPage + ']').addClass('active');
			$('li[data-page],button[data-page]').on('click', function() {
				var page = this.getAttribute('data-page');
				location.href = '/?page=' + page;
			})
		})
	</script>
</body>
</html>