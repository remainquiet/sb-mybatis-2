<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<table class="table table-bordered">
	<tr>
		<th>이름</th>
		<td id="name"></td>
	</tr>
	<tr>
		<th>주소</th>
		<td id="addr"></td>
	</tr>
	<tr>
		<th>비고</th>
		<td id="etc"></td>
	</tr>
	<tr>
		<th colspan="2"><button id="tiUpdate" onclick="tiUpdate()">수정</button>
			<button id="tiDelete" onclick="tiDelete()">삭제</button></th>
	</tr>
</table>
<script>
		$(document).ready(function() {
			$.ajax({
				url : '/test',
				method : 'GET',
				data : 'tiNum=' + ${param.num},
				success : function(res) {
					console.log(res);
					$('#name').html(res.tiName);
					$('#addr').html(res.tiAddr);
					$('#etc').html('<textarea id="tiEtc" maxlength="1000" cols="30" rows="10" readonly>' + res.tiEtc + '</textarea>');

				}
			})
		})
		
		function tiUpdate(){
			$.ajax({
				url : '/test',
				method : 'GET',
				data : 'tiNum=' + ${param.num},
				success : function(res) {
					
					console.log(res);
					$('#name').html('<input type="text" id="tiName" maxlength="100" value="' + res.tiName + '">');
					$('#addr').html('<input type="text" id="tiAddr" maxlength="200" value="' + res.tiAddr + '">');
					$('#etc').html('<textarea id="tiEtc" maxlength="1000" cols="30" rows="10">' + res.tiEtc + '</textarea>">');
					$('#tiDelete').hide();
					
					$('#tiUpdate').on('click',function(){
						var tiName = $('#tiName').val();
						var tiAddr = $('#tiAddr').val();
						var tiEtc = $('#tiEtc').val();
						if (tiName.trim().length < 2) {
							alert('이름은 2글자 이상이여야 합니다');
							return false;
						}
						if (tiAddr.trim().length < 10) {
							alert('주소는 10글자 이상이여야 합니다');
							return false;
						}
						$.ajax({
							url : '/update',
							method : 'POST',
							contentType : 'application/json; charset=utf-8',
							data : JSON.stringify({
								tiNum : res.tiNum,
								tiName : tiName,
								tiAddr : tiAddr,
								tiEtc : tiEtc
							}),
							success : function(res){
								alert(res.msg);
								location.href= '/?page=test-list';
							}
						})
					})

				}
			})
		}
		
		function tiDelete(){
			$.ajax({
				url : '/delete',
				method : 'GET',
				data : 'tiNum=' + ${param.num},
				success : function(res) {
					console.log(res);
					alert(res.msg);
					location.href= '/?page=test-list';

				}
			})
		}


	</script>