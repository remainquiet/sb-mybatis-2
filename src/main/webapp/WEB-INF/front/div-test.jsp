<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html id="html">
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
.container {
	box-sizing: content-box;
	width : 100%;
}

#div1 {
	box-sizing: content-box;
	border: 0;
	margin: 0;
	padding: 0;
	width: 50%;
	background-color: blue;
}

#div2 {
	box-sizing: content-box;
	border: 0;
	margin: 0;
	padding: 0;
	width: 50%;
	background-color: red;
}
</style>
</head>
<body>
	<div class="container">
		<div id="div1">1</div>
		<div id="div2">2</div>
	</div>
</body>
</html>