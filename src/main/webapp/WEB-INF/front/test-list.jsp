<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div class="container">
	<table class="table table-bordered">
		<tr>
			<th class="test"><input type="checkbox" id="chk"></th>
			<th>번호</th>
			<th>이름</th>
			<th>주소</th>
			<th>비고</th>
		</tr>
		<tbody id="tBody">

		</tbody>
	</table>
</div>
<nav aria-label="Page navigation example">
	<ul class="pagination justify-content-center" id="pagination">

	</ul>
	<button id="test-insert" data-page="test-insert">게시물작성</button>
	<button onclick="deleteCheck()">삭제</button>
	<button id="selectCheck" onclick="selectCheck()">선택</button>
</nav>
<script>

function selectCheck(){
	$('.test').show();
	$('#test-insert').hide();
	$('#selectCheck').attr('onclick','').unbind('click');
	$('#selectCheck').html('취소');
	$('#selectCheck').on('click',function(){
		$('#selectCheck').attr('onclick','').unbind('click');
		$('.test').hide();
		$('#test-insert').show();
		$('#selectCheck').html('선택');
		$('#selectCheck').on('click',function(){
			selectCheck();
		})
	})
	
}

$('#chk').click(function(){
	if($('input:checkbox[id="chk"]').is(':checked')){
		$('input:checkbox').prop('checked',true);
	}else{
		$('input:checkbox').prop('checked',false);
	}
})

function deleteCheck(){

		var msg = '체크된 항목들이 전부 삭제 됩니다 정말 삭제하시겠습니까?';

		if(confirm(msg)!=0){
			deleteTestInfo();

		} else{
			alert('삭제가 취소 되었습니다');
		}
	}

function deleteTestInfo(){
	
		var tiNums = [];
		$('input:checkbox[name=checkValue]:checked').each(function(){
			tiNums.push($(this).val());
		})

		$.ajax({
				url : '/delete',
				method : 'GET',
				data : 'tiNums=' + tiNums,
				success : function(res) {
					console.log(res);
					alert(res.msg);
					location.href= '/?page=test-list';

				}
			})
	}


var rowCount = 10;
	$(document).ready(function() {
		goPage(1);
	})
	
	function goPage(p){
		$.ajax({
			url : '/tests',
			method : 'GET',
//			contentType: "application/json; charset=utf-8",
//			data : { "page" : { "page" : p }},
			data : 'page.page=' + p,
			success : function(res) {
				console.log(res);
				var html = '';
				for (var idx of res.list) {
					html += '<tr>';
					html += '<td class="test"><input type="checkbox" name="checkValue" value="' + idx.tiNum + '"></td>'
					html += '<td>' + idx.tiNum + '</td>';
					html += '<td><a href="/?page=view&num=' + idx.tiNum + '">' + idx.tiName + '</a></td>';
					html += '<td>' + idx.tiAddr + '</td>';
					html += '<td>' + idx.tiEtc + '</td>';
					html += '</tr>';
				}
				
				$('#tBody').html(html);
				$('.test').hide();
				var totalPage = Math.ceil(res.page.totalCount/rowCount);
				var sPage = Math.floor((res.page.page-1)/10)*10 + 1;
				var ePage = sPage + 9;
				var numberHtml = '';
				if(ePage>totalPage){
					ePage = totalPage;
				}
				if(res.page.page!=1){
					if(sPage==1){
						numberHtml = '<li class="page-item"><a data-page="' + sPage + '" class="page-link" aria-label="Previous"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a> </li>';
					}else{
						numberHtml = '<li class="page-item"><a data-page="' + (sPage-1) + '" class="page-link" aria-label="Previous"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a> </li>';
					}
					numberHtml += '<li class="page-item"><a data-page="' + (res.page.page-1) + '" class="page-link" aria-label="Previous"><span aria-hidden="true">이전</span><span class="sr-only">Previous</span></a> </li>';
				}
				for(var i = sPage; i<=ePage; i++){
					if(i==res.page.page){
						numberHtml += '<li class="page-item active"><a class="page-link">' + i + '</a></li>';
					}else{
					numberHtml += '<li class="page-item"><a class="page-link" data-page="'+i+'">' + i + '</a></li>';
					}
				}
				if(res.page.page!=totalPage){
					numberHtml += '<li class="page-item"><a data-page="' + (res.page.page+1) + '" class="page-link" aria-label="Previous"><span aria-hidden="true">다음</span><span class="sr-only">Previous</span></a> </li>';
					if(ePage==totalPage){
						numberHtml += '<li class="page-item"><a data-page="' + ePage + '" class="page-link" aria-label="Next"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>';
					}else{
						numberHtml += '<li class="page-item"><a data-page="' + (ePage+1) + '" class="page-link" aria-label="Next"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>';
					}
				}
				$('#pagination').html(numberHtml);
				setEvent();
			}
		})
	}
	function setEvent(){
		$('a[data-page]').on('click',function(){
			goPage(this.getAttribute('data-page'));
		})
	}
</script>