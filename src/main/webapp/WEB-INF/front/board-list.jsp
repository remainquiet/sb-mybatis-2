<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="/js/jquery-3.4.1.min.js"></script>
<script src="/js/bootstrap.bundle.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/css/bootstrap-grid.min.css">
<link rel="stylesheet" href="/css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="/css/bootstrap.min.css">
</head>
<body>
	<div id="enters"></div>
	<script>
	
	function load(page){
			var data = 'page=' + page + '&pageSize=10';
			var html = '';
			$.ajax({
				url : '/boardinfos',
				method : 'GET',
				data : data,
				success : function(res) {
					for (var li of res) {
						html += '<h1>' + li.biTitle + '</h1>';
						html += '<h4> 글쓴이 : ' + li.biWriter + '</h4>';
						if(li.biFile){
							html += '<img src="/resources/' + li.biFile + '"><br>';
						}
						html += '<div class="content"> 글쓴이 : ' + li.biContent + '</div>';
					}
					$("#enters").append(html);
				}
			})
	}
	var page = 1;
	$(document).ready(function() {
		if($("body").height()<$(window).height()){
		load(page);
		}
	})
	
	$(window).scroll(function() {
				if ($(window).scrollTop() == $(document).height() - $(window).height()) {
					console.log(++page);
					if(!load(page)){
						return false;
					}
				}
			});
	</script>
</body>
</html>