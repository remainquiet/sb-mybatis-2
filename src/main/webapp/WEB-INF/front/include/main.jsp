<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%=request.getParameter("page")%><br>
param-page = ${param.page}<br>
	<select id="page" onchange="changePage(this)">
		<option value=""
		<c:if test="${empty param.page}">
			selected
		</c:if>>선택</option>
		<option id="r1" value="right1.jsp"
		<c:if test="${param.page eq 'right1.jsp'}">
			selected
		</c:if>>라이트1</option>
		<option id="r2" value="right2.jsp"
		<c:if test="${param.page eq 'right2.jsp'}">
			selected
		</c:if>>라이트2</option>
	</select><br>
	<c:if test="${not empty param.page}">
		<jsp:include page="/WEB-INF/front/include/${param.page}" />
	</c:if>
	
	<button onclick="">라이트1</button>
	<button onclick="">라이트2</button>
	<script>
		function changePage(f) {
			location.href = '/front/include/main?page=' + f.value;
		}
	</script>
</body>
</html>